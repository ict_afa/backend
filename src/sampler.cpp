/* 
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com
*/

#include <Arduino.h>
#include <sampler.h>

int results[MAX_RESULTS];
volatile int resultNumber;
volatile int delayVal;

void setupSampler()
{
  Serial.println();
  Serial.println("Starting sampler setup...");

  // reset Timer 1
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1 = 0;
  TCCR1B = bit(CS11) | bit(WGM12); // CTC, prescaler of 8
  TIMSK1 = bit(OCIE1B);
  OCR1A = 39;
  OCR1B = 39; // 20 uS - sampling frequency 50 kHz

  ADCSRA = bit(ADEN) | bit(ADIE) | bit(ADIF); // turn ADC on, want interrupt on completion
  //  ADCSRA |= bit (ADPS2);  // Prescaler of 16
  //  ADCSRA |= bit (ADPS0);  // Prescaler of 2
  ADCSRA |= bit(ADPS1); // Prescaler of 4
  //  ADCSRA |= (1 << ADPS1) | (1 << ADPS0);    // 8 prescaler for 153.8 KHz
  ADMUX = bit(REFS0) | (adcPin & 7);
  ADCSRB = bit(ADTS0) | bit(ADTS2); // Timer/Counter1 Compare Match B
  ADCSRA |= bit(ADATE);             // turn on automatic triggering
  delay(500);
  Serial.println("Sampler setup succesfully!");
}

// ADC complete ISR
ISR(ADC_vect)
{
  results[resultNumber++] = ADC;
  delayMicroseconds(delayVal);
}

EMPTY_INTERRUPT(TIMER1_COMPB_vect);

void scopeSampler()
{
  Serial.print("Here!\t");
  Serial.println(resultNumber);
  if (resultNumber >= MAX_RESULTS)
  {
    ADCSRA = 0; // turn off ADC
    for (int i = 0; i < MAX_RESULTS; i++)
    {
      Serial.println(results[i]);
    }
    resultNumber = 0;
    ADCSRA = bit(ADEN) | bit(ADIE) | bit(ADIF) | bit(ADPS2) | bit(ADATE); // turn ADC back on
    memset(results, 0, MAX_RESULTS);
  }
}
