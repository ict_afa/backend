/* 
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com
*/

#ifndef MAIN_H
#define MAIN_H

extern long waveFreq;
extern short waveType, oscPin;

void setup();

void loop();

void setWave(int slavePinChoice, int waveform, long frequency);

#endif