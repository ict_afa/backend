/* 
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com
*/

#ifndef SAMPLER_H
#define SAMPELR_H

const int MAX_RESULTS = 256;
const short adcPin = 0; // A0

extern int results[MAX_RESULTS];
extern volatile int resultNumber;
extern volatile int delayVal;

void setupSampler();

void scopeSampler();

#endif