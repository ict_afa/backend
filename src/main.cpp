/* 
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com
*/

#include <Arduino.h>
#include <SPI.h>
#include "main.h"
#include "sampler.h"
#include "constants.h"

/* 
  ?FSYNC - 53M, 10U
  ?SDATA - 51M, 11U
  ?SCLK -  52M, 13U
*/

long waveFreq;
short waveType, oscPin;

void setup()
{
  // initialize serial communications and setup sampling interrupt routine
  pinMode(LFO_OSC, OUTPUT);
  pinMode(MAIN_OSC, OUTPUT);

  Serial.begin(1000000);
  Serial.println("Initializing setup...");

  SPI.begin(); // initialise SPI

  waveFreq = 100;
  waveType = 0;
  oscPin = 0;

  waveFreq = 1000;
  delayVal = 10;
  setupSampler();

  Serial.println("Initialization complete.");

  delay(1000);
}

void loop()
{
  while (true)
  {
    Serial.println("Waiting for wave definition...");
    scopeSampler();
    if (Serial.available())
    {
      waveFreq = Serial.parseInt();
      if (waveFreq != 0)
      {
        if (waveFreq < 0)
          break;
        else
        {
          delayVal = 10;
          waveType = waveFreq % 10;
          waveFreq /= 10;
          oscPin = waveFreq % 10;
          waveFreq /= 10;
          delayVal = max(1, 10000 / waveFreq);
          Serial.print(oscPin);
          Serial.print('\t');
          Serial.print(waveType);
          Serial.print('\t');
          Serial.println(waveFreq);
          switch (waveType)
          {
          default:
            setWave(oscPin, SINE, waveFreq);
            break;
          case 1:
            setWave(oscPin, SQUARE, waveFreq);
            break;
          case 2:
            setWave(oscPin, TRIANGLE, waveFreq);
            break;
          }
          memset(results, 0, MAX_RESULTS);
        }
      }
    }
  }

  Serial.println("Process Terminated!\nIdle for 5s");
  delay(5000);
  Serial.println("Done!");
  waveFreq = 100000;
  waveType = 0;
}

/*
  Command: set the waveform and frequency (Hz)
*/
void setWave(int slavePinChoice, int waveform, long frequency)
{

  long freq_data = frequency * pow(2, 28) / REF_FREQ;
  int freq_MSB = (int)(freq_data >> 14) | FREQ0;
  int freq_LSB = (int)(freq_data & 0x3FFF) | FREQ0;

  SPI.beginTransaction(SPISettings(SPI_CLOCK_SPEED, MSBFIRST, SPI_MODE2));
  digitalWrite(MAIN_OSC, HIGH);
  digitalWrite(LFO_OSC, HIGH);

  if (slavePinChoice == 0)
  {
    digitalWrite(MAIN_OSC, LOW);

    SPI.transfer16(CR_B28_COMBINED | CR_FSELECT_0 | CR_PSELECT_0 | CR_RESET);
    SPI.transfer16(freq_LSB);
    SPI.transfer16(freq_MSB);
    SPI.transfer16(PHASE0);
    SPI.transfer16(waveform);
    delay(1000);
    digitalWrite(MAIN_OSC, HIGH);
  }
  else
  {
    //    digitalWrite(MAIN_OSC, LOW);
    delay(1);
    digitalWrite(LFO_OSC, LOW);
    SPI.transfer16(CR_B28_COMBINED | CR_FSELECT_0 | CR_PSELECT_0 | CR_RESET);
    SPI.transfer16(freq_LSB);
    SPI.transfer16(freq_MSB);
    SPI.transfer16(PHASE0);
    SPI.transfer16(waveform);
    Serial.print("Selected = ");
    Serial.println("LFO!");
    delay(1000);
    digitalWrite(LFO_OSC, HIGH);
    delay(1);
    //    digitalWrite(MAIN_OSC, LOW);
  }

  SPI.endTransaction();
}

/*
  Command: turn off the waveform output
*/
//void setWaveOff(int slavePin) {
//  SPI.beginTransaction(SPISettings(SPI_CLOCK_SPEED, MSBFIRST, SPI_MODE2));
//  digitalWrite(slavePin, LOW);
//
//  SPI.transfer16(CR_RESET | CR_SLEEP1 | CR_SLEEP12);
//
//  digitalWrite(slavePin, HIGH);
//  SPI.endTransaction();
//}
