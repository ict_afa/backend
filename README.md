# Arduino Backend for In-Circuit Tester (ICT) 

The firmware communicates with the [Python frontend](https://gitlab.com/ict_afa/frontend) (on a computer) via serial communication and serves two primary purposes:
- Driving two AD9833 ICs for generating audio signals and LFOs of the chosen frequency and waveform (sine, sawtooth, square or triangle); It receives the user choice via the frontend.
- Sampling up to 10 test points and relaying the incoming waveforms to the backend; Since we are dealing with audio signals, the sampling rate is set at around 48 Khz. 

This code is not suitable for the [Arduino Nano](https://store.arduino.cc/products/arduino-uno-rev3). It has been developed for the [Arduino Mega 2560](https://store.arduino.cc/products/arduino-mega-2560-rev3) microcontroller.

## Installation

Clone this repo to local, switch to repo

```sh
git clone https://gitlab.com/ict_afa/backend.git

cd backend
```
## Usage

### Prerequisite

[PlatformIO](https://platformio.org/) must be installed in the system. This can either be done via the CLI or by using IDEs like [VS Code](https://code.visualstudio.com/).

### Loading onto Arduino Mega 2560

If using CLI

```sh
platformio run --target upload
```
If using IDEs like VS Code

```
Click on PlatformIO menu on the left sidebar. Click on megaatmega2560>General>Upload
```

## Contributing

0. [Report Bugs or Request Features](https://gitlab.com/ict_afa/backend/-/issues)
1. Fork it (<https://gitlab.com/ict_afa/backend>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Merge/Pull Request

## License

Distributed under the GNU General Public License v3.0 License. See `LICENSE` for more information.
